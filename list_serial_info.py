#!/usr/bin/env python

# pip install pyserial
import serial.tools.list_ports


def main():
    print 'starting {}\n'.format(__file__.split('/')[-1])

    for port in serial.tools.list_ports.comports():
        if 'USB' not in port.device:
            print 'skipping: {}'.format(port.device)
            continue
        printDeviceInfo(port)
        print
# end main


def printDeviceInfo(port):
    print 'device:', port.device
    print 'product:', port.product
    print 'serial_number:', port.serial_number
    print 'product id:', hex(port.pid)
    print 'vendor id:', hex(port.vid)
    print 'manufacturer:', port.manufacturer


if __name__ == '__main__':
    main()

