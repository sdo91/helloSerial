# helloSerial


```
usage: hello_serial.py [-h] [-p SERIAL_PORT] [-b BAUDRATE] [-f FREQUENCY] [-r]

optional arguments:
  -h, --help            show this help message and exit
  -p SERIAL_PORT, --serial_port SERIAL_PORT
                        specify serial port
  -b BAUDRATE, --baudrate BAUDRATE
                        specify baudrate
  -f FREQUENCY, --frequency FREQUENCY
                        frequency in hz
  -r, --readonly        readonly
```


