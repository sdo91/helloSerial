#!/usr/bin/env python





import serial
import argparse
from time import time, sleep





serialPort = None
args = None





"""
usage: see README file
"""
def main():
    print 'starting {}'.format(__file__.split('/')[-1])

    ''' parse arguments '''
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--serial_port', default='/dev/ttyUSB0', help='specify serial port')
    parser.add_argument('-b', '--baudrate', default=115200, type=int, help='specify baudrate')
    parser.add_argument('-f', '--frequency', default=1.0, type=float, help='frequency in hz')
    parser.add_argument('-r', '--readonly', action='store_true', help='read only')
    parser.add_argument('-t', '--timestamp', action='store_true', help='show timestamps')
    global args
    args = parser.parse_args()

    print 'Serial port:', args.serial_port
    if not args.serial_port:
        print 'need a serial port'
        assert False
    print 'Baudrate:', args.baudrate

    global serialPort
    serialPort = serial.Serial(port=args.serial_port, baudrate=args.baudrate)

    if args.readonly:
        print 'listening for serial data, readonly mode'
        while True:
            if serialPort.in_waiting > 0:
                sleep(0.01)  # wait for more bytes
                data = serialPort.read(serialPort.in_waiting)
                handleReceived(data)
            sleep(.01)
    else:
        count = 1
        period = 1.0 / args.frequency
        # print 'period: {}'.format(period)
        prevSend_timestamp = 0
        nextSend_timestamp = 0
        while True:
            ### receive data ###
            if serialPort.in_waiting > 0:
                if nextSend_timestamp == 0:
                    # queue next send
                    nextSend_timestamp = time() + period
                sleep(0.01)  # wait for more bytes
                data = serialPort.read(serialPort.in_waiting)
                handleReceived(data)

            ### send data ###
            if isTimeToSend(period, prevSend_timestamp, nextSend_timestamp):
                msg = 'hello serial {}'.format(count)
                serialPort.write(msg)
                prevSend_timestamp = time()
                nextSend_timestamp = 0
                count += 1
                print 'sent: {}{}'.format(msg, formatTime())

            sleep(0.01)
        # end while

# end main


def isTimeToSend(period, prevSend_timestamp, nextSend_timestamp):
    """
    it is time to send if:
        we have reached nextSend_timestamp
        send has timed out
    """
    current_timestamp = time()
    timeSinceSend = current_timestamp - prevSend_timestamp

    if nextSend_timestamp != 0 and current_timestamp > nextSend_timestamp:
        # we have reached nextSend_timestamp
        return True
    elif timeSinceSend > (3 * period):
        # send has timed out
        return True
    return False


def formatTime():
    if args.timestamp:
        return ' (t = {:.2f})'.format(time() % 1000)
    else:
        return ''


def isTtyPort(port):
    return 'tty' in port


def printBytes(byteString):
    for c in byteString:
        print hex(ord(c)),
    print '(len: {})'.format(len(byteString))


def handleReceived(byteString):
    if args.readonly:
        prefix = '\n'
    else:
        prefix = '\t' * 4
    print '{}received: {}{}'.format(prefix, byteString, formatTime())






if __name__ == '__main__':
    main()



